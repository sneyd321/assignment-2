﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2
{
    public class Bet
    {
        private int _amount;
        private int _dog;
        private Guy _bettor;

        public Bet(int amount, int dog)
        {
            _amount = amount;
            _dog = dog;
        }

        private string getDescription()
        {
            //Return a string that says who placed the bet, how much
            //cash was bet, and which dog he bet on ("Joe bets 8 on
            //dog #4"). If the amount is zero, no bet was placed 
            //("Joe hasn't placed a bet")
            return "";
        }

        private int payOut(int winner)
        {
            //The parameter is the winner of the race. If the dog won,
            //return the amount bet. Otherwise, return the negative of
            //the amount bet.
            return 0;
        }
    }
}
